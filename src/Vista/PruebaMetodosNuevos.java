/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;
import Modelo.Vendedor;
import Negocio.SistemaVentas;
import java.io.IOException;
import Util.LeerMatriz_Excel;



/**
 *
 * @author madar
 */
public class PruebaMetodosNuevos {
    
   public static void main(String[] args) throws IOException {
        SistemaVentas mySistema=new SistemaVentas("src/Datos/vendedores.xls");
        //Crea un array de tipo Vendedor con los vendedores con ventas superiores al promedio
        Vendedor vendedores[] = mySistema.getVendedores_MasVentas();
        //Se llama el metodo para imprimir los vendedores
        imprimir(vendedores);
        //Crea el objeto vendedorMenor y almacena el vendedor que obtuvo la menor venta acumulada
        Vendedor vendedorMenor = mySistema.getMenosVentas();
        //Se llama el metodo para imprimir al Vendedor
        imprimirVendedorMenosVenta(vendedorMenor);
        //Se imprimen las menores ventas de cada vendedor
        System.out.println(mySistema.getVenta_Menor());
    }
    
   /**
    * Metodo que imprime los vendedores con ventas superiores al promedio
    * @param vendedores 
    */
    public static void imprimir(Vendedor [] vendedores){
        String msg = "Vendedores con una venta mayor al promedio: ";
        for(Vendedor v : vendedores){
            msg += "\n" + v.toString();
        }
        System.out.println(msg);
    }
    /**
     * Recibe el vendedor que obtuvo la menor venta acumulada y lo imprime
     * @param vendedorMenor obejto de tipo Vendedor 
     */
    public static void imprimirVendedorMenosVenta(Vendedor vendedorMenor){
        System.out.println("Vendedor que obtuvo la menor venta acumulada" + vendedorMenor.toString());
    }
}
