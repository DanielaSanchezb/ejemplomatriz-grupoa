/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Vendedor;
import Util.LeerMatriz_Excel;
import java.io.IOException;

/**
 * Clase del sistema de ventas
 * @author madarme
 */
public class SistemaVentas {
    
    private Vendedor equipoVentas[];

    public SistemaVentas() {
     
    }

    public SistemaVentas(int numVendedores) {
        
        this.equipoVentas=new Vendedor[numVendedores];
     
    }
    
    /**
     *  Constructor que carga los vendedores a partir de un excel
     * @param rutaArchivo un string con la ruta y el nombre del archivo Ejemplo: src/Datos/vendedores.xls
     * @throws IOException Genera excepción cuando el archivo no existe
     */
    
    public SistemaVentas(String rutaArchivo) throws IOException
    {
        LeerMatriz_Excel myExcel=new LeerMatriz_Excel(rutaArchivo,0);
        String datos[][]=myExcel.getMatriz();
        
        // Normalizar --> Pasar de la matriz de String al modelo del negocio (equipoVentas con cada uno de sus vendedores
        this.equipoVentas=new Vendedor[datos.length-1];
        crearVendedores(datos);
    
    }
    
    
    private void crearVendedores(String datos[][])
    {
    
     for(int fila=1;fila<datos.length;fila++)
     {
         //Crear un Vendedor
         Vendedor nuevo=new Vendedor();
         //Vector de ventas: Creando el espacio
         float ventas[]=new float[datos[fila].length-2];
         
         int indice_venta=0;
         
         for(int columna=0;columna<datos[fila].length;columna++)
         {
         
             if(columna==0)
                 nuevo.setCedula(Long.parseLong(datos[fila][columna]));
             else
             {
                 if(columna==1)
                     nuevo.setNombre(datos[fila][columna]);
                 else //Default
                 {
                     ventas[indice_venta]=Float.parseFloat(datos[fila][columna]);
                     indice_venta++;
                 }
             }
             
         }
         //Asignar el vector de ventas al vendedor:
         nuevo.setVentas(ventas);
         //Asingar el nuevo vendedor al equipo de vendedores:
         this.equipoVentas[fila-1]=nuevo;
      }
    
    }
    
    public Vendedor[] getEquipoVentas() {
        return equipoVentas;
    }

    public void setEquipoVentas(Vendedor[] equipoVentas) {
        this.equipoVentas = equipoVentas;
    }

    @Override
    public String toString() {
        
        String msg="";
            for(Vendedor myVendedor:this.equipoVentas)
                     msg+=myVendedor.toString()+"\n";
        
        return msg;
        
    }
    /**
     * Calcula el promedio total de las ventas de todos los vendedores
     * @return Un float con el promedio de las ventas
     */
        
      public float promedioTotalVentas (){
      float sumarPromedios = 0.0f;
      float promedioTotal = 0.0f;
      //Recorremos el equipo de Ventas para calcular el prmedio de cada uno y sumarlo
      for(int i=0 ; i < equipoVentas.length ; i++)
      sumarPromedios += equipoVentas[i].calcularPromedioVentas();
      //Calculamos el promedio final de todas las ventas
      promedioTotal = sumarPromedios / equipoVentas.length;
      return promedioTotal;
     }
    
    /**
     * Una colección de vendedores que obtuvieron ventas mayores al promedio total de ventas
     * @return  una colección de vendedores
     */
    public Vendedor[] getVendedores_MasVentas()
    {   
        //Creamos un arreglo temporal con el tamaño de todos los vendedores
        float promedioTotal = promedioTotalVentas();
        Vendedor vendedoresTemp[] = new Vendedor[equipoVentas.length];
        int contador = 0;
        //Recorremos todo el equipo de ventas para comparar sus ventas con el promedio general
        for (int i = 0; i < equipoVentas.length; i++) {
            int j = 0;
            while (j < equipoVentas[i].getVentas().length) {
                if (equipoVentas[i].getVentas()[j] > promedioTotal) {
                    //Si su venta "i" es mayor al promedio Total, entonces es añadido al arreglo
                    vendedoresTemp[i] = equipoVentas[i];
                    contador++;
                }
                j++;
            }
        }
        //Finalmente llenamos el arreglo con los datos conocidos
        Vendedor vendedores[] = new Vendedor[contador];
        vendedores = vendedoresTemp;
        return vendedores;
    }

    /**
     * Obtiene el nombre de la venta que obtuvo la menor ganancia
     * venta1, ... venta6
     * @return 
     */
    
        public float sumaColumna(int n){
        float suma = 0.0f;
        
        for(int i=0; i<equipoVentas.length ; ++i)
            suma += equipoVentas[i].getVentaPosicion(n);
        
        return suma;
    }
    
    public String getVenta_Menor()
    {
        int contador = 1;
        int tamañoFilas = equipoVentas[0].getVentas().length;
        float menor = sumaColumna(0);
        for(int i=1 ; i < tamañoFilas ; ++i){
            if(sumaColumna(i) < menor){
                menor = sumaColumna(i);
                contador = i+1;
            }
        }
        return "La menor venta fué: venta" + contador;
    }
    
   /**
    * Obtiene el vendedor que obtuvo la menor venta acumulada
    * @return un objeto de tipo vendedor
    */

    
    public Vendedor getMenosVentas()
    {
        Vendedor vendedorMenor = null;
        //Tomamos el valor float mas grande para asegurar que el menor sea la primera posicion
        float menor = Float.MAX_VALUE;
        float suma = 0.0f;
        //Recorremos el equipo Ventas para comparar e ir tomando el menor
        for(int i=0; i < equipoVentas.length; i++){
            suma = equipoVentas[i].sumarVentas();
            if(suma<menor){
                vendedorMenor = equipoVentas[i];
                menor = suma;
            }
        }
        return vendedorMenor;
    }
}
